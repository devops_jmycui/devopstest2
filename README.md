# A simple calculator node module

# Modified this file in Devopstest2 to trigger the pipleline at Apr 14th 2018 after done the same thing in Devopstest1
# The pipeline doesn't auto trigger, you need to enable it to let the auto run after each commitment.

# 2018.4.15 try to deploy this repo to a container in my AWS server ec2-35-182-104-99.ca-central-1.compute.amazonaws.com


Currently supported operators:

* addition (+)
* subtraction (-)
* multiplication (*)
* division (/)

Limitations:

* No support for grouping with parentheses
* You need to separate your numbers and operators with spaces
* Order of operations is a bit dodgy

### Usage

```
const Calculator = require('calculator')

const calc = new Calculator()
console.log(calc.eval("1 + 1"))    # -> 2
```

